**----------- Représentation des différents objets -----------**

On représente le réseau routier comme un agrégat des noeuds qui le constituent :

**Réseau**

*Agrégat de Noeud*


Dans le réseau un noeud désigne soit un véhicule soit une infrastructure :

**Noeud**

*Classe mère de : Infrastructure, Véhicule*

Les infrastructures possèdent un identifiant et une position :

**Infrastructure**  

*Hérite de Noeud*
  
    Identifiant : interger #unique
    Longitude : integer
    Latitude : integer

Un véhicule est identifié par son immatriculation et possède d'autres champs. Il en existe plusieurs types :
    
**Véhicule** 

*Hérite de Noeud*
    
    Immatriculation : integer #unique 
    Marque : string 
    Modèle : string 
    Année_Production : integer
    
*Classe mère de Voiture, Camion, Moto, Spéciaux*

Une voiture est un type de véhicule :

**Voiture**

*Herite de Véhicule*

Un camion est un type de véhicules composé au plus d'une capacité maximale de d'objets Voiture :

**Camion**

    Capacité Max : integer

*Hérite de Véhicule*

Une moto est un type de véhicule. Cet objet possède une information sur la capacité du moteur :

**Moto**

*Hérite de Véhicule*

    Capacité_Moteur : integer
    
Les véhicules spéciaux sont ceux qui ne rentrent pas dans les catégories précédentes (ex : pompiers, ambulance, ..). On les caractérise par leur type :   
    
**Spéciaux**

*Hérite de Véhicule*

    Type : string
    
Une commune est identifiée par son nom qu'on suppose unique pour le réseau. Elle possède aussi un code postal :
    
**Commune**

    Modèle : string
    Numéro_Série : integer
    
Un capteur est identifié par le modèle et un numéro de série associé :
    
**Capteur**

    Modèle : string
    Numéro_Série : integer
    
Un évènement est une notion abstraite que l'on sépare en plusieurs types :
    
**Evenement**

*Classe abstraite mère de Accident, Alerte Météo, Travaux, Contrôle*

Un accident a une gravité et concerne des véhicules :

**Accident**

*Hérite de Evenement*

    Gravité : string
    Nombre_Véhicules : integer
    Types_Véhicules : list
    
L'alerte météo témoigne d'un temps et d'une température :

**Alerte Météo **

*Hérite de Evenement*

    Temps : string
    Température : integer
    
**Travaux**

*Hérite de Evenement*

**Contrôle**

*Hérite de Evenement*

Un échange entre 2 noeuds possèdent un horodatage, est soit de type un-à-un soit de type un-à-plusieurs :

**Echange**

    Timestamp : timestamp
    Type : {un-à-un, un-à-plusieurs}
    
*Agrégat de Evenement, Information Véhicule*

On renseigne la position d'un véhicule :

**Information Véhicule**

    Longitude : integer
    Latitude : integer
    
*Agrégat de Véhicule, Capteur*

**----------- Représentation des différentes associations -----------**

**Véhicule - Information Véhicule**

Chaque véhicule dispose d'une position sur le réseau qui est la plus récente communiquée à la base de données. On suppose qu'elle a pour valeur par défaut la position d'entrée sur le réseau.
Ainsi un véhicule a toujours une seule position et cette position concerne uniquement ce véhicule. Si la voiture disparaît sa position aussi :

    Composition : Véhicule "1" *-- "1" Information Véhicule
    
**Camion - Voiture**

Un camion peut transporté jusqu'à un nombre N de voitures où N est égal à la capacité maximale du camion. 
Une voiture ne peut pas être transportée par plusieurs camions en même temps. Si le camion disparaît les voitures qu'il transporte aussi :

    Composition : Camion "1" *-- "0..Capacité Max" Voiture
    
**Echange - Information Véhicule**

Un échange peut contenir la position d'un véhicule s'il émet l'échange. Il en contient au plus une car il n'y a pas d'échange possible entre 2 véhicules.
Une position de véhicule peut ne pas être transmise si le véhicule n'émet jamais d'échanges. Elle peut être transmise à plusieurs échanges si le véhicule a gardé sa position.

    Association : Echange "*" o-- "0..1" Information Véhicule : contient >
    
**Echange - Evenement**

Un échange concerne toujours un évènement et un seul. Un évènement peut être le sujet de différents échanges :

    Association : Echange "*" o-- "1" Evenement : contient >
    
**Noeud - Echange**

Un noeud peut émettre plusieurs échange ou aucun. Un échange à toujours un et un seul noeud émetteur.

    Association : Noeud "1" -- "*" Echange : émet >
    
Un noeud peut recevoir plusieurs échanges ou aucun. Un échange à toujours un noeud destinataire voire plusieurs dans le cas d'un échange de type un-à-plusieurs :   
   
    Association : Noeud "1..*" -- "*" Echange : < reçoit
    
**Infrastructure - Commune**

Une infrastructure est toujours reliée à une et une seule commune. Une commune peut être reliée à aucne ou plusieurs infrastructures :

    Association : Infrastructure "*" -- "1" Commune : est relié à >
    
**Véhicule - Capteur**

Un véhicule peut posséder plusieurs ou aucun capteur. Un capteur ne peut être installé que sur un véhicule à la fois :

    Association : Véhicule "1" -- "*" Capteur : possède > 
    
**Infrastructure - Capteur**

Une infrastructure possède au moins un capteur. Un capteur ne peut être installé que sur une infrastructure à la fois :

    Association : Infrastructure "1" -- "1..*" Capteur : possède >
    
**Noeud - Capteur**

Un capteur peut être ajouté à un seul noeud. Un noeud peut se faire ajouter plusieurs ou aucun capteur :

    Association : Capteur "*" -- "1" Noeud : est ajouté à >
    
Un capteur peut être transféré à un seul noeud. Un noeud peut se faire transférer plusieurs ou aucun capteur :
    
    Association : Capteur "*" -- "1" Noeud : est transféré à >
    
Un capteur peut être transféré à partir d'un seul noeud. Un noeud peut transférer plusieurs ou aucun capteur :
    
    Association : Capteur "*" -- "1" Noeud : est transféré de >
    
Un capteur peut être supprimé à un seul noeud. Un noeud peut se faire supprimer plusieurs ou aucun capteur :
    
    Association : Capteur "*" -- "1" Noeud : est supprimé de > 
    
**Capteur - Evenement**

Un capteur peut détecter plusieurs ou aucun évènement. Un évènement est détecté par au moins un capteur :

    Association : Capteur "1..*" -- "*" Evenement : détecte >

**Commune - Evenement**

Un évènement concerne une et une seule commune. Une commune peut être la cible de plusieurs ou aucun évènement :

    Association : Evenement "*" -- "1" Commune : concerne >

**----------- Représentation utilisateurs -----------**

On prévoit 3 utilisateurs :

    Utilisateurs du réseau : peuvent seulement renseigner la position de leur véhicules
    Police : peut chercher dans la BDD les différents véhicules 
    Maintenance du réseau : peut effectuer tous types d'opérations sur les capteurs et les stations de base.

**----------- Vues et requêtes prévues -----------**

Vue 1 : Lister tous les véhicules dans une région (définie par un encadrement de longitude et latitude)

    Retourner les véhicules référencés dans Information Véhicule dont les latitude et longitude sont comprises dans l'encadrement.

Vue 2 : Lister toutes les communications liées à un véhicule ou à une station de base

    Retourner toutes les communications dont l'émetteur ou le destinataire est le véhicule ou la station.
    
Vue 3 : Lister les positions des véhciules d'un certain modèle.

    Retourner les positions des véhicules spécifiés.
    
Vue 4 : Lister les positions des véhciules d'un certain type.

    Retourner les positions des véhicules spécifiés.

Vue 5 : Lister les communications qui concernent un modèle de capteur

    Retourner les communications dont l'évènement a été détecté par le modèle de capteur.
    

**----------- Contraintes -----------**

Pour les relations d'héritage entre Noeud (classe mère) et Infrastructure et Véhiucle (classes filles) l'héritage est exclusif.

Il en est de même pour les classes héritant de Véhicule.


Si un véhicule émet un échange celui-ci est de type un-à-un.

Si un noeud émet un échange, l'évènement dont il est question doit avoir été détecté par un capteur du dit noeud.


Un capteur est possédé exclusivement par une infrastrcuture ou un véhicule (XOR).

Si un capteur est transféré d'un noeud il doit être transféré à un autre et inversement (AND).
